# Use this file to export all diff data to text files

import csvdiff
import json
import re
import os
import sys

rootdir = os.path.abspath(os.path.dirname(sys.argv[0])) + '\\'

iteration = 24
strip_spaces_regex = re.compile(r'\s+')

text_file_header_template = '''
CHANGES FROM CASE WITHOUT PV PLANT TO CASE WITH PV PLANT FOR HOUR {hour}h
'''

bus_template = ''' 

Bus: {bus} 
'''

changed_variable_template = '''{fieldKey} ====> From: ({valueFrom}) -> To: ({valueTo})
'''


class Diff:
    def __init__(self, result_folder_with_solar: str,
                 result_folder_without_solar: str, diff_result_folder: str):
        self.is_stationary = False
        self.result_folder_with_solar = rootdir + result_folder_with_solar
        self.result_folder_without_solar = rootdir + result_folder_without_solar
        self.diff_result_folder = rootdir + diff_result_folder

    def strip_spaces(self, input):
        return strip_spaces_regex.sub('', str(input))

    def diff(self, hour, from_file: str, to_file: str, diff_target_file):
        patch = csvdiff.diff_files(from_file, to_file, ['Bus'])

        text_file_header = text_file_header_template.format(hour=hour)

        str_friendly = ''

        for change in patch['changed']:
            bus = change['key'][0]
            fields_changed = change['fields']

            # Friendly formatting of results
            str_friendly = str_friendly + bus_template.format(bus=bus)
            for fieldKey in fields_changed.keys():
                str_friendly = str_friendly + changed_variable_template.format(
                    fieldKey=fieldKey,
                    valueFrom=self.strip_spaces(
                        fields_changed[fieldKey]['from']),
                    valueTo=self.strip_spaces(fields_changed[fieldKey]['to']))

        if str_friendly:
            text_file = text_file_header + str_friendly
            print(text_file, file=open(diff_target_file, 'w'))

    def get_folder_name(self, name: str):
        if (self.is_stationary):
            template = '''{old_name}_stacionarno'''
            return template.format(old_name=name)
        return name

    def run_diff(self):
        self.is_stationary = True
        hour = 'stacionarno'
        self.diff(
            hour, '{folder_without}\\hour_without_{hour}.csv'.format(
                hour=hour,
                folder_without=self.get_folder_name(
                    self.result_folder_without_solar)),
            '{folder_with}\\hour_with_{hour}.csv'.format(
                hour=hour,
                folder_with=self.get_folder_name(
                    self.result_folder_with_solar)),
            '{diff_result_folder}\\hour_{hour}.txt'.format(
                hour=hour,
                diff_result_folder=self.get_folder_name(
                    self.diff_result_folder)))

        self.is_stationary = False
        for i in range(iteration):
            self.diff(
                str(i + 1), '{folder_without}\\hour_without_{hour}.csv'.format(
                    hour=str(i + 1),
                    folder_without=self.get_folder_name(
                        self.result_folder_without_solar)),
                '{folder_with}\\hour_with_{hour}.csv'.format(
                    hour=str(i + 1),
                    folder_with=self.get_folder_name(
                        self.result_folder_with_solar)),
                '{diff_result_folder}\\hour_{hour}.txt'.format(
                    hour=str(i + 1),
                    diff_result_folder=self.get_folder_name(
                        self.diff_result_folder)))
