import csvdiff
import win32com.client as win32
import os
import shutil
import sys

# Call COM object dynamically
dssObj = win32.gencache.EnsureDispatch('OpenDSSEngine.DSS')
rootdir = os.path.abspath(os.path.dirname(sys.argv[0])) + '\\'

dssText = dssObj.Text


class Simulation:
    def __init__(self, dssObj, ieee34ModFileName: str,
                 withSolarResultFolderName: str,
                 withoutSolarResultFolderName: str, numberOfIterations: int,
                 voltage_profile_no_solar: str, voltage_profile_solar: str):
        self.dssObj = dssObj
        self.dssText = dssObj.Text
        self.numberOfIterations = numberOfIterations
        self.ieee34ModFileName = ieee34ModFileName
        self.withSolarResultFolderName = withSolarResultFolderName
        self.withoutSolarResultFolderName = withoutSolarResultFolderName
        self.voltage_profile_no_solar = voltage_profile_no_solar
        self.voltage_profile_solar = voltage_profile_solar
        self.is_stationary = False
        self.run_file_path = rootdir + 'Run_IEEE34Mod2.dss'

    # Check if it's solar power plant by checking for string in file
    def is_solar(self):
        return not '! Redirect PVSystem_Model.dss' in open(
            self.ieee34ModFileName).read()

    def file_name(self, hour: str):
        solar = self.is_solar()
        name_infix = 'with' if solar else 'without'
        folder_to_save = self.withSolarResultFolderName if solar else self.withoutSolarResultFolderName
        folder_to_save = self.get_folder_name(folder_to_save)

        file_name = rootdir + '{folderName}\\hour_{infix}_{hour}.csv'.format(
            folderName=folder_to_save, infix=name_infix, hour=hour)

        return file_name

    def get_folder_name(self, name: str):
        if (self.is_stationary):
            template = '''{old_name}_stacionarno'''
            return template.format(old_name=name)
        return name

    def voltage_profile_save(self, hour: str):
        solar = self.is_solar()
        folder_to_save = self.voltage_profile_solar if solar else self.voltage_profile_no_solar
        folder_to_save = self.get_folder_name(folder_to_save)

        return self.get_file_name(folder_to_save, hour)

    def get_file_name(self, folder_to_save: str, hour: str):
        solar = self.is_solar()
        name_infix = 'with' if solar else 'without'

        if self.is_stationary:
            file_name = rootdir + '{folderName}\\{infix}_{hour}.csv'.format(
                folderName=folder_to_save, infix=name_infix, hour=hour)
        else:
            file_name = rootdir + '{folderName}\\hour_{infix}_{hour}.csv'.format(
                folderName=folder_to_save, infix=name_infix, hour=hour)

        return file_name

    def cleanup_folders(self):
        folders_to_cleanup = [
            'Rezultati_sa_elektranom', 'Rezultati_bez_elektrane'
        ]
        for folder in folders_to_cleanup:
            for file in folder:
                file_path = os.path.join(folder, file)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                except Exception as e:
                    print(e)

    def setup_dss_init(self):
        # Clear OpenDSS engine of all handles
        self.dssText.Command = 'Clear'

        # Compile main run file
        self.dssText.Command = 'Compile (' + self.run_file_path + ')'

    def setup_dss_daily_mode(self):
        self.dssText.Command = 'Set mode=daily Number=1'
        self.dssText.Command = 'set stepsize=1h'
        self.dssText.Command = 'set maxcontroliter=300'

    def setup_dss_snapshot_mode(self):
        self.dssText.Command = 'Set mode=snapshot'
        self.dssText.Command = 'set maxcontroliter=300'

    def setup_dss_meters(self):
        self.dssText.Command = 'New Energymeter.M1  Line.L1  1'
        if self.is_solar():
            self.dssText.Command = 'New Monitor.PVmonitor Element=PVSystem.PV1 mode=1'

    def setup_dss_solve(self, hour: str):
        self.dssText.Command = 'solve'
        # self.dssText.Commmand = 'Buscoords IEEE34_BusXY.csv'
        self.dssText.Command = 'Interpolate'

        # Export voltages to csv
        # Export voltages really does this for 1 point in time. If set number=24 that means it's for hour 23
        self.dssText.Command = 'Export Voltages (' + self.file_name(hour) + ')'
        self.dssText.Command = 'Export Profile Phases=all (' + \
            self.voltage_profile_save(hour) + ')'

    def run_simulation(self):
        self.is_stationary = False
        self.setup_dss_init()

        # Set daily mode
        self.setup_dss_daily_mode()
        self.setup_dss_meters()
        for i in range(self.numberOfIterations):
            self.setup_dss_solve(str(i + 1))
            if i == 23:
                if self.is_solar():
                    self.dssText.Command = 'Plot Monitor Object=PVMonitor Channels=[1,3,5]'

        self.is_stationary = True

        # Set snapshot mode
        self.setup_dss_init()

        self.setup_dss_snapshot_mode()
        self.setup_dss_meters()
        # Solve circuit
        self.setup_dss_solve('stacionarno')
        self.dssText.Command = 'Plot Profile Phases=all'
