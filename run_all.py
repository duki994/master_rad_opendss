import win32com.client as win32
from simulation import Simulation
from resultdiff import Diff
import re
import os
import sys

dssObj = win32.gencache.EnsureDispatch('OpenDSSEngine.DSS')
dssText = dssObj.Text

#pylint: disable-msg=too-many-arguments
simulation = Simulation(dssObj, 'ieee34Mod2.dss', 'Rezultati_sa_elektranom',
                        'Rezultati_bez_elektrane', 24,
                        'Naponski_profili_bez_elektrane',
                        'Naponski_profili_sa_elektranom')
#pylint: enable-msg=too-many-arguments

rootdir = os.path.abspath(os.path.dirname(sys.argv[0])) + '\\'

diff = Diff('Rezultati_sa_elektranom', 'Rezultati_bez_elektrane',
            'Promene_u_velicinama')


class Run:
    def __init__(self, simulation: Simulation, diff: Diff,
                 ieee34mod2_file_name: str, regex_without_solar,
                 regex_with_solar):
        self.regex_without_solar = regex_without_solar
        self.regex_with_solar = regex_with_solar
        self.simulation = simulation
        self.ieee34mod2_file_name = rootdir + ieee34mod2_file_name
        self.diff = diff

    def run_without_solar(self):
        # find if it's with solar
        regex_replace = re.compile(self.regex_with_solar)
        self.replace_ieee34_file(regex_replace,
                                 '! Redirect PVSystem_Model.dss')
        self.simulation.run_simulation()
        return

    def run_with_solar(self):
        # find if it's with solar
        regex_replace = re.compile(self.regex_without_solar)
        self.replace_ieee34_file(regex_replace, 'Redirect PVSystem_Model.dss')
        self.simulation.run_simulation()
        return

    def run_all(self):
        self.run_with_solar()
        self.run_without_solar()
        self.diff.run_diff()
        return

    def replace_ieee34_file(self, regex_replace, to_replace: str):
        mod_file = open(self.ieee34mod2_file_name, 'r').read()
        mod_file = regex_replace.sub(to_replace, mod_file)
        open(self.ieee34mod2_file_name, 'w').write(mod_file)


run = Run(simulation, diff, 'ieee34Mod2.dss', r'! Redirect PVSystem_Model.dss',
          r'Redirect PVSystem_Model.dss')

# run.run_with_solar()
# run.run_without_solar()

run.run_all()

exit()